package com.amen.sda.test;

import com.amen.sda.test.N21_10.shelfs.ProductType;

import java.util.Scanner;

public class MainEnum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        do {
            String line = scanner.nextLine();

            try {
                ProductType type = ProductType.valueOf(line);

                System.out.println("Typ : " + type);
            }catch (IllegalArgumentException iae){
                System.out.println(iae.getMessage());
            }
        } while (scanner.hasNextLine());
    }
}
