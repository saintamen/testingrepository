package com.amen.sda.test.N21_10.shelfs;

import java.util.*;

public class Registry {
    private Map<ProductClass, List<Product>> mapClass = new HashMap<>();
    private Map<ProductType, List<Product>> mapType = new HashMap<>();

    public Registry() {
        for (ProductClass classType : ProductClass.values()) {
            mapClass.put(classType, new LinkedList<>());
        }
        for (ProductType productType : ProductType.values()) {
            mapType.put(productType, new LinkedList<>());
        }
    }

    public void addProduct(Product toAdd) {
        // <<<<<<<<<<<<<< dodanie do mapy 1
        // to co pobralem, to wszystkie produkty (ich lista)
        // tej samej klasy co produkt dodawany
        List<Product> listaProduktow = mapClass.get(toAdd.getProductClass());
//        if (listaProduktow == null) {
//            listaProduktow = new LinkedList<>();
//        }
        listaProduktow.add(toAdd);

        mapClass.put(toAdd.getProductClass(), listaProduktow);

        // <<<<<<<<<<<<<< dodanie do mapy2
        listaProduktow = mapType.get(toAdd.getProductType()); // mapa typow
        listaProduktow.add(toAdd);

        mapType.put(toAdd.getProductType(), listaProduktow);
    }

    public void printClasses() {
        // iterujemy pary klucz wartość - entry
        for (Map.Entry<ProductClass, List<Product>> entry : mapClass.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    public void printTypes() {
        // iterujemy pary klucz wartość - entry
        for (Map.Entry<ProductType, List<Product>> entry : mapType.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    public void printType(ProductType type) {
        System.out.println(mapType.get(type));
    }

    public void printClass(ProductClass productClass) {
        System.out.println(mapClass.get(productClass));
    }
}
