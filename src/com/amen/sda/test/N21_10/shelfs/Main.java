package com.amen.sda.test.N21_10.shelfs;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Registry registry = new Registry();
//        registry.addProduct(new Product("Bulka",
//                2.50,
//                ProductClass.LOW,
//                ProductType.FOOD));
//
//        registry.printClasses();
//
//        registry.addProduct(new Product("Bulka2",
//                2.30,
//                ProductClass.LOW,
//                ProductType.FOOD));
//        registry.printClasses();

        Scanner sc = new Scanner(System.in);
        boolean isWorking = true;

        while(isWorking){

            String line = sc.nextLine();

            String toLowerTrimmed = line.toLowerCase().trim();

            if (toLowerTrimmed.startsWith("dodaj ")){
                String[] words = toLowerTrimmed.replace("dodaj ", "").split(" ");

                Product newProduct = new Product();
                newProduct.setName(words[0]); // nazwa
                newProduct.setPrice(Double.parseDouble(words[1])); // cena
                newProduct.setProductClass(ProductClass.valueOf(words[2].toUpperCase())); // zamieniam na same duze
                newProduct.setProductType(ProductType.valueOf(words[3].toUpperCase())); // zamieniam na same duze

                registry.addProduct(newProduct);
            }else if(toLowerTrimmed.startsWith("wylistuj_typ ")){
                String[] words = toLowerTrimmed.replace("wylistuj_typ ", "").split(" ");

                ProductType type = ProductType.valueOf(words[0].toUpperCase());

                registry.printType(type);

            }else if (toLowerTrimmed.startsWith("wylistuj_polka ")) {
                String[] words = toLowerTrimmed.replace("wylistuj_polka ", "").split(" ");

                ProductClass productClass= ProductClass.valueOf(words[0].toUpperCase());

                registry.printClass(productClass);
            }else if(toLowerTrimmed.equals("quit")){
                break;
            }
        }


    }
}
