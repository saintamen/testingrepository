package com.amen.sda.test.N21_10.shelfs;

public class Product {
    private String name;
    private double price;

    // opcjonalnie
    private ProductClass productClass;
    private ProductType productType;

    public Product(String name, double price, ProductClass productClass, ProductType productType) {
        this.name = name;
        this.price = price;
        this.productClass = productClass;
        this.productType = productType;
    }

    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ProductClass getProductClass() {
        return productClass;
    }

    public void setProductClass(ProductClass productClass) {
        this.productClass = productClass;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", productClass=" + productClass +
                ", productType=" + productType +
                '}';
    }
}
